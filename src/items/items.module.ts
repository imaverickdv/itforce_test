import { CacheModule, Module } from '@nestjs/common';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import { DbModule } from "../db/db.module";
import { UsersModule } from "../users/users.module";
import { HttpModule } from "@nestjs/axios";

@Module({
  controllers: [ItemsController],
  providers: [ItemsService],
  imports: [
      DbModule,
      UsersModule,
      HttpModule,
      CacheModule.register()
  ]
})
export class ItemsModule {}
