import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { HttpService } from "@nestjs/axios";
import { firstValueFrom, map } from "rxjs";
import { Cache } from 'cache-manager';
import * as crypto from "crypto";

@Injectable()
export class ItemsService {
    constructor(
        @Inject(CACHE_MANAGER) private cacheManager: Cache,
        private httpService: HttpService
    ) {}

    async getItems(): Promise<Map<string, object>> {
        const request = 'https://api.skinport.com/v1/items?app_id=730&currency=USD';
        const cacheKey = crypto.createHash('md5').update(request).digest('hex');
        const cached: Map<string, object> = await this.cacheManager.get(cacheKey);

        if (cached) {
            console.log('From cache');
            return cached;
        }

        const items = new Map();

        try {
            await Promise.all([
                firstValueFrom(
                    this.httpService
                        .get(request + '&tradable=0')
                        .pipe(map(res => res.data))
                ),
                firstValueFrom(
                    this.httpService
                        .get(request + '&tradable=1')
                        .pipe(map(res => res.data))
                )
            ]).then(async ([res1, res2]) => {
                // Отрефакторил с учетом того, что массивы могут отличаться.
                // Например, между запросами может прогореть кэш
                res1.forEach(item => {
                    items.set(item['market_hash_name'], item);
                });

                for (let item of res2) {
                    const key = item['market_hash_name'];

                    if (!items.has(key)) {
                        // Обрабатываем согласно тех. заданию
                        return;
                    }

                    items.set(key, {...items.get(key), min_price_tradable: item['min_price']});
                }

                await this.cacheManager.set(cacheKey, items, 100000);
            });
        } catch (error) {
            console.error(error);
        }

        return items;
    }
}
