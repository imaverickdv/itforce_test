import {Controller, Get, Param} from '@nestjs/common';
import {DbService} from "../db/db.service";
import {UsersService} from "../users/users.service";
import {ItemsService} from "./items.service";

@Controller('items')
export class ItemsController {
    constructor(
        private dbService: DbService,
        private itemsService: ItemsService,
        private usersService: UsersService,
    ) {
    }

    @Get()
    async getItems() {
        const result = (await this.itemsService.getItems()).values();
        // Если нужно вернуть именно массив
        return Array.from(result);
    }

    @Get('/:itemId/buy')
    async buyItem(@Param('itemId') itemId: number) {
        await this.usersService.pay(1, 10);

        return 'Operation succeed';
    }
}
