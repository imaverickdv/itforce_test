import { BadRequestException, Injectable } from '@nestjs/common';
import { DbService } from "../db/db.service";

@Injectable()
export class UsersService {
    constructor(private dbService: DbService) {}

    async pay(userId: number, amount: number) {
        try {
            await this.dbService.begin();

            const [user] = await this.dbService.executeQuery(
                'SELECT id, balance::numeric FROM users WHERE id = $1 FOR UPDATE',
                [userId]
            );

            if (!user) {
                throw new BadRequestException('user not found');
            }

            if (user.balance < amount) {
                throw new BadRequestException('insufficient funds');
            }

            await this.dbService.executeQuery(
                'UPDATE users SET balance = balance - $1 WHERE id = $2;',
                [amount, userId]
            );

            await this.dbService.commit();

            return true;
        } catch (error) {
            await this.dbService.rollback();
            throw error;
        }
    }
}
